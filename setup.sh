#!/bin/bash -e

set -o pipefail

[ "$NAME" ]       || NAME=
[ "$EMAIL" ]      || EMAIL=
[ "$IRCSET" ]     || IRCSET=
[ "$IRCSERVER" ]  || IRCSERVER=irc.freenode.net
[ "$IRCPORT" ]    || IRCPORT=6667
[ "$IRCNICK" ]    || IRCNICK=coward
[ "$IRCPASS" ]    || IRCPASS=
[ "$DOTFILES" ]   || DOTFILES="$HOME"/.dotfiles

if [[ -f "$DOTFILES"/dotfilesrc ]]; then
	source "$DOTFILES"/dotfilesrc
fi

die() { echo "$1" 1>&2 && show_help && [ "$2" ] && [ "$2" -ge 0 ] && exit "$2" || exit 1; }

# Show help function to be used below
show_help() {
	echo "USAGE: setup.sh [arguments]"
	echo "ARGS:"
	cat <<HELP
   -h, --help       # This help message
   -n, --name       # Name for git
   -e, --email      # Email for git
   -i, --irssi      # Do not prompt for irssi options
   -s, --server     # Server for irssi
   -p, --port       # Port for irssi
   -N, --nick       # Nick for irssi
   -P, --password   # Password for irssi
HELP
}

# Parse command line options (odd formatting to simplify show_help() above)
NARGS=-1; while [ "$#" -ne "$NARGS" ]; do NARGS=$#; case $1 in
	# SWITCHES
	-h|--help)       # This help message
		show_help; exit 1; ;;
	-n|--name)       # Name for git
		shift && NAME="$1" && shift; ;;
	-e|--email)      # Email for git
		shift && EMAIL="$1" && shift; ;;
	-i|--irssi)
		shift && IRCSET=1 && shift; ;;
	-s|--server)     # server for irssi
		shift && IRCSERVER="$1" && IRCSET=1 && shift; ;;
	-p|--port)       # port for irssi
		shift && IRCPORT="$1" && IRCSET=1 && shift; ;;
	-N|--nick)       # nick for irssi
		shift && IRCNICK="$1" && IRCSET=1 && shift; ;;
	-P|--password)   # password for irssi
		shift && IRCPASS="$1" && IRCSET=1 && shift; ;;
esac; done

if [[ ! "$NAME" ]]; then
	read -e -p "Git author name [$USER]: " NAME
fi
[ "$NAME" ] || NAME="$USER"
if [[ ! "$EMAIL" ]]; then
	read -e -p "Git author email [$USER@$(hostname)]: " EMAIL
fi
[ "$EMAIL" ] || EMAIL="$USER@$(hostname)"
if [[ ! "$IRCSET" ]]; then
	read -e -p "Would you like to set up irssi [y/N]? " yn
	case $yn in
		[Yy]* ) 
			read -e -p "irssi server [$IRCSERVER]: " IRCSERVER 
			read -e -p "irssi port [$IRCPORT]: " IRCPORT
			read -e -p "irssi nick [$IRCNICK]: " IRCNICK
			read -e -p "irssi psasword []: " IRCPASS
			;;
	esac
fi

fn_distro(){
	if [[ -f /etc/lsb-release ]]; then
		os=$(lsb_release -s -d)
	elif [[ -f /etc/debian_version ]]; then
		os="Debian $(cat /etc/debian_version)"
	elif [[ -f /etc/redhat-release ]]; then
		os=$(cat /etc/redhat-release)
	else
		os="$(uname -s) $(uname -r)"
	fi
}

UNAMESTR=$(uname)
if [[ "$UNAMESTR" == 'Linux' ]]; then
	fn_distro
	if [[ "$os" == Ubuntu* ]]; then
		sudo apt-get install -y build-essential git irssi vim-nox zsh tmux
		[ -d "$HOME"/go ] || curl -L https://storage.googleapis.com/golang/go1.4.2.linux-amd64.tar.gz | tar -xzC "$HOME"
	elif [[ "$os" == CentOS* ]]; then
		sudo yum install -y git zsh ruby vim-enhanced
	fi
elif [[ "$UNAMESTR" == 'Darwin' ]]; then
	# Probably need to set up brew and brew install some stuff here...
	[ -d "$HOME"/go ] || curl -L https://storage.googleapis.com/golang/go1.4.2.darwin-amd64-osx10.8.tar.gz | tar -xzC "$HOME"
fi

export GOROOT="$HOME"/go
export GOPATH=$HOME

if [[ ! -d "$DOTFILES" ]]; then
	git clone http://git.chrissexton.org/cws/dotfiles.git "$DOTFILES"
elif [[ ! -d "$DOTFILES"/files ]]; then
	die "You must have a destination for the dotfiles repository!"
fi

cat >"$DOTFILES"/dotfilesrc <<DOTFILESRC
NAME="$NAME"
EMAIL="$EMAIL"
IRCSET=1
IRCSERVER="$IRCSERVER"
IRCPORT="$IRCPORT"
IRCNICK="$IRCNICK"
IRCPASS="$IRCPASS"
DOTFILES="$DOTFILES"
DOTFILESRC


cat "$DOTFILES"/files/gitignore_global > "$HOME"/.gitignore_global

<"$DOTFILES"/files/gitconfig sed -e "s/%NAME%/$NAME/" | sed -e "s/%EMAIL%/$EMAIL/" > "$HOME"/.gitconfig

cat "$DOTFILES"/files/tmux.conf > "$HOME"/.tmux.conf

cat "$DOTFILES"/files/vimrc > "$HOME"/.vimrc
if [[ "$os" == CentOS* ]]; then
	sudo ln -s /usr/bin/vim /usr/local/bin/vi
fi

if [[ ! -d "$HOME"/.oh-my-zsh ]]; then
	curl -L https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh
fi

if [[ ! "$SHELL" =~ .*zsh ]]; then
	if [[ "$os" == CentOS* ]]; then
		chsh -s /bin/zsh
	else
		chsh -s "$(which zsh)"
	fi
fi

cat "$DOTFILES"/files/zshrc > "$HOME"/.zshrc

mkdir -p "$HOME"/bin
cat "$DOTFILES"/files/tmux.sh > "$HOME"/bin/tmux.sh

mkdir -p "$HOME"/.irssi
<"$DOTFILES"/files/irssiconfig sed -e "s/%PASSWORD%/$IRCPASS/" | sed -e "s/%NICK%/$IRCNICK/" | sed -e "s/%SERVER%/$IRCSERVER/" | sed -e "s/%PORT%/$IRCPORT/" > "$HOME"/.irssi/config

mkdir -p "$HOME"/.emacs.d
cat "$DOTFILES"/files/init.el > "$HOME"/.emacs.d/init.el

if [[ ! -L "$HOME"/bin/setup.sh ]]; then
	ln -s "$DOTFILES"/setup.sh "$HOME"/bin
fi
