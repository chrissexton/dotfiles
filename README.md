# Dotfiles!

This is my repository of files needed for general setup of my machines. It includes nice defaults for Dvorak users for vim and tmux, and also sets up a configuration for other commonly used programs.

## Included configurations:

* Git
* tmux
* VIM
* ZSH
* irssi

## Installation

`bash <( curl -L http://git.chrissexton.org/cws/dotfiles/raw/master/setup.sh)`

Or if you want to customize without an interactive prompt:

`bash <(curl -L http://git.chrissexton.org/cws/dotfiles/raw/master/setup.sh) -n "<<YOUR NAME>>" -e "<<YOUR EMAIL>>" -i`

You may customize the irssi configuration with the `server`, `port`, `nick`, and `password` options. Any of these options may be specified by environment variables instead of switches.  

## Prerequisites

Why list prereqs before the installation command? Becasue they're likely not to matter.

* Ubuntu or OSX
* git/tmux/irssi under OSX (no auto brew setup yet)
* curl
